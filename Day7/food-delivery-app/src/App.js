import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import SearchBar from "./components/SearchBar/SearchBar";
import Home from "./pages/Home/Home";


class App extends Component {
  render() {
    return (

      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
          <Switch>
                <Route exact path= "/" render={() => (
                  <Redirect to="/home"/>
                )}/>
              <Route exact path='/home' component={Home} />
          </Switch>
      </div>
    </Router>
    );
  }
}

export default App;
