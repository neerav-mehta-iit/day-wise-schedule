import React, { Component } from 'react';
import './Home.css';
import SearchBar from "../../components/SearchBar/SearchBar";
import QuickSearch from "../../components/QuickSearch/QuickSearch";

class Home extends Component {
    constructor() {
        super();
        this.state = {
            locationList: [],
            restaurantList: []
        }
    }

    onTypeAhead() {
        fetch('https://demo0614424.mockable.io/getRestaurantList')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({"restaurantList": data})
            });
    }

    componentDidMount() {
        fetch('https://demo0614424.mockable.io/locationList')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({"locationList": data})
            });
    }

    render() {
        return (<div className='home-container'>
                <SearchBar locationList={this.state.locationList} onTypeAhead={this.onTypeAhead.bind(this)} restaurantList={this.state.restaurantList}/>
                <QuickSearch />
            </div>
        );
    }
}

export default Home;
