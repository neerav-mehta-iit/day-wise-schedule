import React, { Component } from 'react';
import './Home.css';
import restaurantList from "./RestaurantList";
import locationList from "./LocationList";
import './QuickSearch.css';
import './QuickSearchTile.css';

class App extends Component {
  render() {
    return (<div className='App home-container'>
          <div className='quick-search-container container-fluid '>
            <div className='container'>
              <div className='quick-search-heading row'>Quick Searches</div>
              <div className='quick-search-subheading row'>Discover restaurants by type of meal</div>
              <div className='row'>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/breakfast.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Breakfast'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/lunch.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Lunch'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/dinner.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Dinner'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/snacks.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Snacks'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/drinks.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Drinks'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
                <div className='col-sm-4'>
                  <div className='quick-search-tile-container row'>
                    <div className='col-sm-5 image-container' >
                      <img src={'assets/nightlife.png'}/>
                    </div>
                    <div className='col-sm-7 content-bar' >

                      <div className='content-heading row' >{'Nightlife'}</div>
                      <div className='content-text row' >{'Start your day with exclusive breakfast options'}</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
