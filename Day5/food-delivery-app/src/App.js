import React, { Component } from 'react';
import './App.css';
import SearchBar from "./components/SearchBar/SearchBar";
import Home from "./pages/Home/Home";


class App extends Component {
  render() {
    return (
        <div className='App'>
          <Home />
        </div>
    );
  }
}

export default App;
